package appium.steps;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;

import static appium.pageObject.CadastroCliente.*;

public class CadastroClienteSteps {

    @Dado("^que acesso a tela de cadastro de cliente$")
    public void acessarTelaCadastroDeAluno() {
        //time(20);
        clicarBtnPularHomeScreem();
        clicarBtnCadastrarNovoUsuario();
    }

    @E("^preencho as informações obrigatorias do formulario$")
    public void preencherFormulario() {
        preencherInputNomeCliente();
        down();
        preencherInputApelidoCadastroScreem();
        preencherInputMobileCadastroScreem();
        preencherInputPhoneCadastroScreem();
        swipeRight(1);
        preencherInputCPF();
        clicarBtnRadio();
    }

    @Quando("^confirmo o cadastro$")
    public void clicarEmConfirmar() {

        clicarBtnGravar();
    }

    @Entao("^deve validar um novo cliente cadastrado com lista$")
    public void validarCadastroNaLista() {
        clicarBtnCustomer();
        Assert.assertEquals(retornaNomeClienteLista(), getNomeCliente());
    }

}