package appium.support;

import appium.connect.App;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static appium.config.DefaultValue.*;

public class Element extends App {

    protected static MobileElement element(By by) {
        waitElement(by);
        return appiumDriver.findElement(by);
    }

    protected static List<MobileElement> elements(By by) {
        waitElement(by);
        return appiumDriver.findElements(by);
    }

    protected static boolean exist(By by) {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        return appiumDriver.findElement(by).isDisplayed();
    }

    protected static boolean elementIsVisible(By by) {
        return appiumDriver.findElement(by).isDisplayed();
    }

    protected static void waitElement(By by) {
        appiumDriver.manage().timeouts().implicitlyWait(TIME_DEFAULT, TimeUnit.SECONDS);
        appiumDriver.findElement(by);
    }

}
