package appium.pageObject;

import appium.support.BaseScreen;
import org.openqa.selenium.By;

public class CadastroCliente extends BaseScreen {

    public static final String NOME_CLIENTE = "Testes Mobile";
    public static final String CPF_CLIENTE = "11228855449";

    private static final By btnPularHomeScreem = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.Button");
    private static final By btnCadastrarNovoUsuarioHomeScreem = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.Button");
    private static final By inputNomeClienteEmpresaCadastroScreem = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.EditText[1]");
    private static final By inputApelidoCadastroScreem = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[2]");
    private static final By inputPhoneCadastroScreem = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[5]");
    private static final By inputMobileCadastroScreem = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[3]");
    private static final By inputCPF = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[1]");

    private static final By btnCustomer = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]");
    private static final By btnRadioM = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.RadioButton[1]");
    private static final By btnGravar = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ImageView");

    private static final By getPrimeiroTextListCliente = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ListView[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]");


    public static void clicarBtnPularHomeScreem(){
        click(btnPularHomeScreem);
    }

    public static void clicarBtnCadastrarNovoUsuario(){
        click(btnCadastrarNovoUsuarioHomeScreem);
    }

    public static void preencherInputNomeCliente(){
        preencherInput(inputNomeClienteEmpresaCadastroScreem, NOME_CLIENTE );
    }

    public static void preencherInputApelidoCadastroScreem(){
        preencherInput(inputApelidoCadastroScreem, "Automatizando" );
    }

    public static void preencherInputMobileCadastroScreem(){
        preencherInput(inputMobileCadastroScreem, "99999999" );
    }

    public static void preencherInputPhoneCadastroScreem(){
        preencherInput(inputPhoneCadastroScreem, "9999-9999" );
    }

    public static void clicarBtnRadio(){
        click(btnRadioM);
    }

    public static void clicarBtnGravar(){
        click(btnGravar);
    }

    public static void down(){
        scrollDown(1);
    }

    public static void clicarBtnCustomer(){
        click(btnCustomer);
    }

    public static String retornaNomeClienteLista(){
        return getText(getPrimeiroTextListCliente);
    }

    public static void preencherInputCPF(){
        preencherInput(inputCPF, CPF_CLIENTE );
    }

    public static String getNomeCliente(){
        return NOME_CLIENTE;
    }

}
